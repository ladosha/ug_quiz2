package ge.msda.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ThirdActivity extends AppCompatActivity {

    private String name = "";
    private int age = 0 ;

    private EditText surnameEditText;
    private Button finishButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            this.name = extras.getString("NAME", "");
            this.age = extras.getInt("AGE", 0);
        }

        surnameEditText = findViewById(R.id.editTextSurname);
        finishButton = findViewById(R.id.buttonFinish);

        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String surname = surnameEditText.getText().toString();

                Intent intent = new Intent(ThirdActivity.this, FinishActivity.class);
                intent.putExtra("AGE", age);
                intent.putExtra("NAME", name);
                intent.putExtra("SURNAME", surname);
                startActivity(intent);

                finish();

            }
        });

    }
}